

														============
														   HR BOT
														============

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */											


								 

# What is HR BOT ? 
===================

HR BOT is a NLP based service which stimulates HCI (Human Computer Interaction) in a way that employees can get all kinds of HR
related queries and support through chatting with the BOT. Queries that employees tend to get from the HR department of a compnay like
Leave Balance, Leave Status, LFA eligibility, ACR reports would be accessible from the HR BOT. If employee intend to apply for a leave,
HR BOT will guide him/her through the entire application process, extract required values from him and applied for the leave on behalf of
the employee automatically. At the same time, management could also track the Leave status, ACR reports for any distinct employee or any
specified timeframe as well.

So, HR BOT actually works like a wrapper over the HR Department of a company providing most common and frequently asked questions and
services to the employees.

HR BOT has been intregated with the HR & Payroll Software developed by ERA-INFOTECH LIMITED. With this extension with the HR & Payroll 
Software employees just have to interact with the bot to get the most frequent automated services of HR in return.    


# Example:
===========

Suppose, a dummy employee named Mr. X has been authenticated.

		BOT: Hello, Mr. X! How can I help you ? 
		USER: Show me my leave balance ? 
		BOT: Sure! What kind of leave ? 
		USER: Casual Leave
		BOT: Your leave balance for casual leave is : 12.
		........

#Features:

	1. Extended Database interaction capability.
	2. Fully supported with Python Request-Response mechanism.
	3. Could respond to multiple intents based on action parameters.
	4. Able to receive request parameters as JSON and prepare response for user intelligently. 
	5. Extract parametric values from user to generate sql query using different algoritms independently. 
	6. Nested requested could be invoked to interact external webservices as well as RESTFUL Web Services.
	7. User Authentication support added.
	8. SSL (Secured Socket Layer) included.
	
	
Regards, 

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	

