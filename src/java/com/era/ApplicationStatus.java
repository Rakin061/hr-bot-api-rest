/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.era;

import com.connect.DBConnectionHandler;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Salman Rakin
 */
@WebServlet(name = "ApplicationStatus", urlPatterns = {"/ApplicationStatus"})
public class ApplicationStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ApplicationStatus</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ApplicationStatus at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sql = null;
        String format = request.getParameter("format");
        String action = request.getParameter("act").trim();

        String emp_id = request.getParameter("id").trim();

        String result = null;
        //String appl_type_code="'AI','DE','AP','AG";
        //String corp_flag="Y";
        //String user_id=request.getParameter("usname");
        //user_id="'"+user_id+"'";
        //String passwd=request.getParameter("paswd");
        //passwd="'"+passwd+"'";
        String company_code = "'001'";

        String leave_type = null;
        String LFA_FLAG = "N";
        String time_frame = null;
        String Error_Flag="";
        String Error_Desc="";
        String access_token="";
        int no_of_employees=0;

        //String query="SELECT DISTINCT appl_status_desc FROM ocasmn.vw_appl_sts_info WHERE application_id = 'DE16030144'";
        out.println(action);
        //out.println(user_id); 
        //out.println(passwd);
        out.println(sql);
        out.println(emp_id);
        //out.println(leave_type);
        //out.println(query_procedure); 
        //String[] res= new String[500];

        ArrayList<String> res = new ArrayList<String>();

        ArrayList<String> approval_person = new ArrayList<String>();

        //for(int i=0;i<60;i++) res[i]=null;
        //String[] res={};
        Connection con = DBConnectionHandler.getConnection();

        try {
            // PreparedStatement ps = con.prepareStatement("SELECT APPL_STATUS_DESC FROM ocasmn.vw_appl_sts_info "
            //       + " WHERE APPLICATION_ID=?");
            //  ps.setString(1, id);

            //ocasmn.dpr_oca_cmse_bot_info('001','shahrin@bankasia.net','123456',out_corporate_flg,out_appl_type_code,out_messages,out_code );
            /*
            CallableStatement statement = con.prepareCall("{call OCASMN.dpr_oca_cmse_bot_info(?, ?, ?, ?, ?, ?, ?)}");
           
            statement.registerOutParameter(4,Types.VARCHAR);
            statement.registerOutParameter(5, Types.VARCHAR);
            statement.registerOutParameter(6, Types.VARCHAR);
            statement.registerOutParameter(7, Types.INTEGER);
            
            statement.setString(1, company_code);
            statement.setString(2, user_id);
            statement.setString(3, passwd);
            
            //statement.execute();
            
           
            String corp_flag = statement.getString(4);
            String appl_type_code = statement.getString(5);
            String out_message = statement.getString(6);
            Integer out_code=statement.getInt(7);
           
            out.println(corp_flag);
            out.println(appl_type_code);
            out.println(out_message);
            out.println(out_code);
            
            statement.close();
          
            
            
            if(action.equals("ApplicationStatus")||action.equals("Proposal.Count_AR")||action.equals("Proposal.Count")){
                 sql=sql+" AND application_type_code IN ("+appl_type_code+") AND createby = DECODE ('"+corp_flag+
                                "','N','"+user_id+"',createby)";
                    
            }
            else if(action.equals("Performance.individual")){
                
                String[] parts= sql.split("GROUP BY", 2);
                
                String str1=parts[0];
                String str2=parts[1];
                
                str1=str1+" AND application_type_code IN ("+appl_type_code+") AND createby = DECODE ('"+corp_flag+
                                "','N','"+user_id+"',createby)";
                str2=" GROUP BY "+ str2;
                
                sql=str1+str2;
                    
            }
            
             */
            
            if (action.equals("employee.info")){
                
                sql="SELECT DISTINCT PYEMPNAM FROM VW_PYEMPMAS WHERE PYEMPCDE='"+emp_id+"'";
            
            }
            else if (action.equals("Leave.02")) {

                sql = "SELECT DESCRIPTION FROM vw_pylevmst WHERE PYEMPCDE='" + emp_id + "'";

            } else if (action.equals("Leave.03")) {

                leave_type = request.getParameter("leave_type").trim();

                if (leave_type.equals("All")) {

                    sql = "SELECT DESCRIPTION,NVL(PYYRLBAL,0) FROM vw_pylevmst WHERE PYEMPCDE='" + emp_id + "'";
                } else {

                    if (leave_type.equals("CL")) {
                        leave_type = "LV013";
                    } else if (leave_type.equals("EL") || leave_type.equals("LFA")) {
                        leave_type = "LV001";
                    } else if (leave_type.equals("SL")) {
                        leave_type = "LV002";
                    } else if (leave_type.equals("ML")) {
                        leave_type = "LV006";
                    }

                    sql = "SELECT DESCRIPTION,NVL(PYYRLBAL,0) FROM vw_pylevmst WHERE PYEMPCDE='" + emp_id + "' AND PYLEVCDE='" + leave_type + "'";
                }
            } else if (action.equals("Leave.08")) {

                sql = "SELECT max(PYDUEDTE) AS LFA_DATE FROM VW_PYLFADUE WHERE PYCOMCDE='200' AND PYEMPCDE='" + emp_id + "' AND NVL(PYAVLFLG,'N')='N'";
            } else if (action.equals("Leave.14")) {

                sql = "SELECT PYDIVCDE v_PYDEPCDE,PYDEPCDE v_PYBRANCD,PYGRDCDE v_PYGRDCDE FROM vw_pyempmas WHERE PYCOMCDE='200' AND PYEMPCDE='" + emp_id + "'";

            } else if (action.equals("Leave.15")) {

                leave_type = request.getParameter("leave_type").trim();
                String start_date = request.getParameter("start_date").trim();
                String end_date = request.getParameter("end_date").trim();

                if (leave_type.equals("All")) {

                    sql = "SELECT PYLEVCDE,PYLFAFLG,SUM(PYCLSDTE-PYSTRDTE+1) tl FROM VW_LEAVE_APP WHERE PYEMPCDE = '" + emp_id
                            + "' AND PYFRMDTE >= TO_DATE('" + start_date + "', 'YYYY-MM-DD')AND PYFRMDTE <= TO_DATE('" + end_date
                            + "', 'YYYY-MM-DD') GROUP BY PYEMPCDE, PYLEVCDE, PYLFAFLG";

                } else {

                    if (leave_type.equals("LFA")) {
                        LFA_FLAG = "Y";
                    }

                    {
                        if (leave_type.equals("CL")) {
                            leave_type = "LV013";
                        } else if (leave_type.equals("EL") || leave_type.equals("LFA")) {
                            leave_type = "LV001";
                        } else if (leave_type.equals("SL")) {
                            leave_type = "LV002";
                        } else if (leave_type.equals("ML")) {
                            leave_type = "LV006";
                        }
                    }

                    sql = "SELECT PYLEVCDE,PYLFAFLG,SUM(PYCLSDTE-PYSTRDTE+1) tl FROM VW_LEAVE_APP WHERE PYEMPCDE = '" + emp_id
                            + "' AND PYFRMDTE >= TO_DATE('" + start_date + "', 'YYYY-MM-DD')AND PYFRMDTE <= TO_DATE('" + end_date
                            + "', 'YYYY-MM-DD')AND PYLEVCDE='" + leave_type + "' AND PYLFAFLG='" + LFA_FLAG + "' GROUP BY PYEMPCDE, PYLEVCDE, PYLFAFLG";

                }

            } else if (action.equals("Leave.16") || action.equals("Leave.17")) {

                time_frame = request.getParameter("time_frame").trim();
                String start_date = request.getParameter("start_date").trim();
                String end_date = request.getParameter("end_date").trim();

                out.println("Time_frame: " + time_frame);
                out.println("Start_date: " + start_date);
                out.println("End_date: " + end_date);
                //start_date="01-01-2017";
                //end_date="01-07-2017";

                //sql = "SELECT PYLEVCDE,PYEMPCDE, PYLFAFLG,PYCLSDTE,PYSTRDTE FROM VW_LEAVE_APP WHERE  PYSTRDTE >= TO_DATE('" + start_date + "', 'MM/DD/YYYY')AND PYCLSDTE <= TO_DATE('" + end_date + "', 'MM/DD/YYYY') ";
                
                CallableStatement statement = con.prepareCall("{call DPR_MOB_LEAVE_SCH_DV_GR(?, ?, ?, ?, ?)}");
                
                statement.setString(1, "200");
                statement.setString(2, emp_id);
                
                statement.registerOutParameter(3, Types.VARCHAR);
                statement.registerOutParameter(4, Types.VARCHAR);
                statement.registerOutParameter(5, Types.VARCHAR);
                
                statement.execute();
                
                Error_Flag= statement.getString(4).trim();
                if (Error_Flag.equals("Y")) access_token= statement.getString(3).trim();
                Error_Desc= statement.getString(5).trim();
                
                out.println("Access Token: "+access_token);
                out.println("Error Flag: "+Error_Flag);
                out.println("Error Desc: "+Error_Desc);
                out.println(Error_Flag.equals("Y"));
                
                
                statement.close();
                
                if (Error_Flag.equals("Y")){
                    
                    sql="SELECT COUNT(DISTINCT PYEMPCDE) FROM VW_LEAVE_APP WHERE PYFRMDTE>=TO_DATE('"+start_date+"','YYYY-MM-DD') AND PYENDDTE <=TO_DATE"
                         + "('"+end_date+"','YYYY-MM-DD') AND PYEMPCDE IN (SELECT EMPCDE FROM EMP_LIST_ACC WHERE TOKEN_NO="+access_token+")";
                    
                    PreparedStatement ps = con.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery();
                    
                    while(rs.next()) no_of_employees=rs.getInt(1);
                    
                    rs.close();
                    ps.close();
                    
                    out.println("Number of employees: "+no_of_employees);
                    
                
                    sql="SELECT PYEMPCDE, PYLEVCDE ,PYLFAFLG, (SELECT PYEMPNAM FROM VW_PYEMPMAS WHERE VW_PYEMPMAS.PYCOMCDE="
                         + "VW_LEAVE_APP.PYCOMCDE AND VW_PYEMPMAS.PYEMPCDE=VW_LEAVE_APP.PYEMPCDE) APPLICANT_NAME,TO_CHAR(PYSTRDTE,'DD-Mon-YYYY'), TO_CHAR(PYCLSDTE,'DD-Mon-YYYY')" 
                         + "    FROM VW_LEAVE_APP WHERE PYFRMDTE>=TO_DATE('"+start_date+"','YYYY-MM-DD') AND PYENDDTE <=TO_DATE"
                         + "('"+end_date+"','YYYY-MM-DD') AND PYEMPCDE IN (SELECT EMPCDE FROM EMP_LIST_ACC WHERE TOKEN_NO="+access_token+")";
                
                } 

                

            } else if (action.equals("Leave.-1")) {

                time_frame = request.getParameter("time_frame").trim();
                String start_date = request.getParameter("start_date").trim();
                String end_date = request.getParameter("end_date").trim();

                out.println("Time_frame: " + time_frame);
                out.println("Start_date: " + start_date);
                out.println("End_date: " + end_date);
                //start_date="01-01-2017";
                //end_date="01-07-2017";

                sql = "SELECT PYLEVCDE,PYEMPCDE, PYLFAFLG,PYCLSDTE,PYSTRDTE FROM VW_LEAVE_APP WHERE  PYSTRDTE >= TO_DATE('" + start_date + "', 'YYYY-MM-DD')AND PYCLSDTE <= TO_DATE('" + end_date + "', 'YYYY-MM-DD') ";

            } else if (action.equals("Lv.App.03")){
                
                String from_date= request.getParameter("from_date").trim();
                String to_date= request.getParameter("to_date").trim();
                
                sql="select count(*) from ORBHRM.PYLEVDTL where PYEMPCDE='"+emp_id+"' and PYLEVSTS='RAS'and ((trunc(PYFRMDTE)  between TO_DATE('"+from_date+"', 'YYYY-MM-DD') AND TO_DATE('"+to_date+"', 'YYYY-MM-DD') ) or  (trunc(PYENDDTE)  between TO_DATE('"+to_date+"', 'YYYY-MM-DD') AND TO_DATE('"+to_date+"', 'YYYY-MM-DD')))";
                                  
            }
            
            else if (action.equals("Lv.App.04")){
                
                String replacement_id= request.getParameter("replacement_id").trim();
                String from_date= request.getParameter("from_date").trim();
                String to_date= request.getParameter("to_date").trim();
                
                sql="select count(*) from ORBHRM.PYLEVDTL where PYEMPCDE='"+replacement_id+"' and PYLEVSTS='RAS'and ((trunc(PYFRMDTE)  between TO_DATE('"+from_date+"', 'YYYY-MM-DD') AND TO_DATE('"+to_date+"', 'YYYY-MM-DD') ) or  (trunc(PYENDDTE)  between TO_DATE('"+to_date+"', 'YYYY-MM-DD') AND TO_DATE('"+to_date+"', 'YYYY-MM-DD')))";
                                  
            }
            else if (action.equals("Lv.App.06")){
                
                leave_type = request.getParameter("leave_type").trim();
                
                if (leave_type.equals("CL")) {
                        leave_type = "LV013";
                    } else if (leave_type.equals("EL") || leave_type.equals("LFA")) {
                        leave_type = "LV001";
                    } else if (leave_type.equals("SL")) {
                        leave_type = "LV002";
                    } else if (leave_type.equals("ML")) {
                        leave_type = "LV006";
                    }
                
                sql="SELECT CONCAT(PYHDRCDE,PYSOFCDE) FROM VW_PYCODMAS WHERE PYHDRCDE = 'LT' AND PYCOSTCN='"+leave_type+"'";
                                  
            }
            
            

            out.println(sql);
            OrderedJSONObject json = new OrderedJSONObject();
            LinkedHashMap<String, Map<String, Object>> map_json = new LinkedHashMap<String, Map<String, Object>>();
            int row_count = 1;
            
            if(sql != null){

                    PreparedStatement ps = con.prepareStatement(sql);
                    
                    ResultSet rs = ps.executeQuery();
                    ResultSetMetaData rsmd = rs.getMetaData();
                    int columnsNumber = rsmd.getColumnCount();
                    int j = 1;

                    int loop_count = 0;

                    while (rs.next()) {
                        //result = rs.getString("APPL_STATUS_DESC");
                        out.println("Entered into Root .....");
                        loop_count++;
                        Map<String, Object> map = new LinkedHashMap<String, Object>();
                        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

                        if (action.equals("Proposal.Count") || action.equals("Proposal.Count_AR")) { // how many proposals approved

                            result = rs.getString("N0_OF_PROPOSAL");
                            map.put("flag", "0");
                            map.put("result", result);
                            //list.add(map);
                            json.put("Status", map);
                            out.println(json);
                        } else if (action.equals("Performance.individual")) {

                            for (int i = 1; i <= columnsNumber; i++) {

                                res.add(rs.getString(i)); //=rs.getString(i);
                                map.put(rsmd.getColumnName(i), res.get(j - 1).trim());
                                j++;

                            }

                            //list.add(map);
                            //map_json.put("Row"+row_count, map);
                            json.put("Row" + row_count, map);

                            row_count++;

                        } else if (action.equals("employee.info")) {

                            result = rs.getString(1);
                            //map.put("Leave"+row_count,result);
                            json.put("Employee_name" , result);
                            row_count++;

                        } else if (action.equals("Leave.02")) {

                            result = rs.getString("DESCRIPTION");
                            //map.put("Leave"+row_count,result);
                            json.put("Leave" + row_count, result);
                            row_count++;

                        } else if (action.equals("Leave.03")) {

                            json.put(rs.getString(1), rs.getString(2));
                            //json.put("Leave"+row_count, map);

                            row_count++;

                        } else if (action.equals("Leave.08")) {

                            json.put("Employee_ID", emp_id);
                            json.put("LFA_DATE", rs.getString(1));
                            //json.put("Leave"+row_count, map);

                            //row_count++;
                        } else if (action.equals("Leave.14")) {

                            leave_type = request.getParameter("leave_type").trim();
                            String start_date = request.getParameter("start_date").trim();
                            String end_date = request.getParameter("end_date").trim();

                            SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");

                            java.util.Date parsed = date_format.parse(start_date);
                            Date sql_start_date = new Date(parsed.getTime());

                            parsed = date_format.parse(end_date);
                            Date sql_end_date = new Date(parsed.getTime());

                            out.println("Employee_id: " + emp_id);
                            out.println("Leave_type " + leave_type);
                            out.println("Start_date " + sql_start_date);
                            out.println("End_date " + sql_end_date);

                            String v_PYDEPCDE = rs.getString(1);
                            String v_PYBRANCD = rs.getString(2);
                            String v_PYGRDCDE = rs.getString(3);
                            String p_PYLEVTYP = null;
                            String p_PYLFAFLG = "N";

                            out.println(v_PYDEPCDE + v_PYBRANCD + v_PYGRDCDE);

                            if (leave_type.equals("CL")) {
                                p_PYLEVTYP = "CL001";
                            } else if (leave_type.equals("EL")) {
                                p_PYLEVTYP = "EL001";
                            } else if (leave_type.equals("SL")) {
                                p_PYLEVTYP = "SL001";
                            } else if (leave_type.equals("ML")) {
                                p_PYLEVTYP = "ML001";
                            } else if (leave_type.equals("LFA")) {
                                p_PYLEVTYP = "EL001";
                                p_PYLFAFLG = "Y";

                            } else {
                                p_PYLEVTYP = "CL001";
                            }

                            CallableStatement statement = con.prepareCall("{call DPR_MOB_LEAVE_APR_SET(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

                            statement.setString(1, "200");
                            statement.setString(2, emp_id);
                            statement.setString(3, p_PYLEVTYP);
                            statement.setString(4, p_PYLFAFLG);
                            statement.setString(5, "");
                            statement.setDate(6, sql_start_date);
                            statement.setDate(7, sql_end_date);
                            statement.setString(8, "N");

                            statement.registerOutParameter(9, Types.VARCHAR);
                            statement.registerOutParameter(10, Types.VARCHAR);
                            statement.registerOutParameter(11, Types.VARCHAR);
                            statement.registerOutParameter(12, Types.VARCHAR);
                            statement.registerOutParameter(13, Types.VARCHAR);
                            statement.registerOutParameter(14, Types.VARCHAR);
                            statement.registerOutParameter(15, Types.VARCHAR);

                            statement.setString(16, v_PYBRANCD);
                            statement.setString(17, v_PYDEPCDE);
                            statement.setString(18, v_PYGRDCDE);

                            statement.registerOutParameter(19, Types.VARCHAR);

                            statement.execute();

                            String out_messege = statement.getString(9);

                            approval_person.add(statement.getString(10));
                            approval_person.add(statement.getString(11));
                            approval_person.add(statement.getString(12));
                            approval_person.add(statement.getString(13));
                            approval_person.add(statement.getString(14));
                            approval_person.add(statement.getString(15));

                            /*
                            String first_approval = statement.getString(10);
                            String second_approval = statement.getString(11);
                            String third_approval = statement.getString(12);
                            String fourth_approval = statement.getString(13);
                            String fifth_approval = statement.getString(14);
                            String sixth_approval = statement.getString(15);

                             */
                            String leave_doc = statement.getString(19);

                            statement.close();

                            /*
                            out.println("Out_message"+ out_messege);
                            out.println("First approval person: "+ first_approval);
                            out.println("Second approval person: "+ second_approval);
                            out.println("Third approval person: "+ third_approval);
                            out.println("Fourth approval person: "+ fourth_approval);
                            out.println("Fifth approval person: "+ fifth_approval);
                            out.println("Six approval person: "+ sixth_approval);
                             */
                            out.println(approval_person);

                            out.println("Leave Doc: " + leave_doc);

                        } else if (action.equals("Leave.15")) {

                            leave_type = rs.getString(1);
                            LFA_FLAG = rs.getString(2);

                            if (leave_type.equals("LV013")) {
                                leave_type = "CL";
                            } else if (leave_type.equals("LV001")) {

                                if (LFA_FLAG.equals("Y")) {
                                    leave_type = "LFA";
                                } else {
                                    leave_type = "EL";
                                }

                            } else if (leave_type.equals("LV002")) {
                                leave_type = "SL";
                            } else if (leave_type.equals("LV006")) {
                                leave_type = "ML";
                            }

                            json.put(leave_type, rs.getString(3));
                            //json.put("Leave"+row_count, map);

                            row_count++;

                        } else if (action.equals("Leave.16") || action.equals("Leave.17")) {

                            OrderedJSONObject json_helper = new OrderedJSONObject();

                            emp_id = rs.getString(1);
                            leave_type = rs.getString(2);
                            LFA_FLAG = rs.getString(3);
                            String emp_name= rs.getString(4);
                            String from_date= rs.getString(5);
                            String to_date= rs.getString(6);

                            if (leave_type.equals("LV013")) {
                                leave_type = "Casual Leave";
                            } else if (leave_type.equals("LV001")) {

                                if (LFA_FLAG.equals("Y")) {
                                    leave_type = "LFA";
                                } else {
                                    leave_type = "Earned Leave";
                                }

                            } else if (leave_type.equals("LV002")) {
                                leave_type = "Special Leave";
                            } else if (leave_type.equals("LV006")) {
                                leave_type = "Maternity Leave";
                            }

                            json_helper.put("Employee ID", emp_id);
                            json_helper.put("Employee Name", emp_name);
                            json_helper.put("Leave type", leave_type);
                            json_helper.put("FROM_DATE", from_date);
                            json_helper.put("TO_DATE", to_date);
                            json.put("Record" + row_count, json_helper);

                            //json.put("Description",json_helper);
                            //json.put("Leave"+row_count, map);
                            row_count++;

                        }

                        else if (action.equals("Lv.App.03")){

                            result=rs.getString(1);

                            json.put("Result",result);


                        }

                        else if (action.equals("Lv.App.04")){


                            result=rs.getString(1);
                            String replacement_id= request.getParameter("replacement_id").trim();
                            String name="";
                            String ID="";
                            row_count=0;


                            if (result.equals("0")){

                                json.put("Result",result);
                                sql="Select distinct Initcap(pyempnam) Name,pyempcde ID \n" +
                                    "From VW_PYEMPMAS a,VW_PYCODMAS b,VW_SYUSRMAS c\n" +
                                    "where a.pycomcde = '200'\n" +
                                    "And a.pycomcde = c.divncode\n" +
                                    "And b.pycomcde = '999'\n" +
                                    "And b.pyhdrcde = 'ST'\n" +
                                    "And nvl(b.pydoppro,'N') = 'Y' \n" +
                                    "And valdflag = 'A'\n" +
                                    "And pydepcde = (select PYDEPCDE from VW_PYEMPMAS where PYEMPCDE='"+emp_id+"')\n" +
                                    "And b.pyhdrcde||b.pysofcde = a.pystatus\n" +
                                    "And a.pyempcde Not In ('"+emp_id+"')\n" +
                                    "And a.pyempcde ='"+replacement_id+"'\n" +
                                    "And PYGRDCDE = (select PYGRDCDE from VW_PYEMPMAS where PYEMPCDE='"+emp_id+"')\n" +
                                    "Order BY 1";

                                ps = con.prepareStatement(sql);
                                rs = ps.executeQuery();

                                while(rs.next()){

                                    name=rs.getString("NAME");
                                    ID= rs.getString("ID");
                                    row_count++;


                                }

                                if(row_count>0){

                                    json.put("Replacement_Name",name);
                                    json.put("Replacement_ID",ID);
                                    json.put("Replacement_Status","in your Own_Dept");

                                }
                                else{

                                    row_count=0;
                                    sql="Select distinct Initcap(pyempnam) Name,pyempcde ID \n" +
                                        "From VW_PYEMPMAS a,VW_PYCODMAS b,VW_SYUSRMAS c\n" +
                                        "where a.pycomcde = '200'\n" +
                                        "And a.pycomcde = c.divncode\n" +
                                        "And b.pycomcde = '999'\n" +
                                        "And b.pyhdrcde = 'ST'\n" +
                                        "And nvl(b.pydoppro,'N') = 'Y' \n" +
                                        "And valdflag = 'A'\n" +
                                        "--And pydepcde = (select PYDEPCDE from VW_PYEMPMAS where PYEMPCDE=:APP_USER)\n" +
                                        "And b.pyhdrcde||b.pysofcde = a.pystatus\n" +
                                        "And a.pyempcde Not In ('"+emp_id+"')\n" +
                                        "And a.pyempcde ='"+replacement_id+"'\n" +
                                        "--And PYGRDCDE = (select PYGRDCDE from VW_PYEMPMAS where PYEMPCDE=:APP_USER)\n" +
                                        "Order BY 1";

                                    ps = con.prepareStatement(sql);
                                    rs = ps.executeQuery();

                                    while(rs.next()){

                                        name=rs.getString("NAME");
                                        ID= rs.getString("ID");
                                        row_count++;


                                    }

                                    if(row_count>0){

                                        json.put("Replacement_Name",name);
                                        json.put("Replacement_ID",ID);
                                        json.put("Replacement_Status","in the Other_Dept");

                                    }
                                    else{

                                        json.put("Replacement_Name",name);
                                        json.put("Replacement_ID",ID);
                                        json.put("Replacement_Status","Inactive");


                                    }



                                }



                            }
                            else{

                                json.put("Result",result);

                                row_count=0;
                                sql="Select distinct Initcap(pyempnam) Name,pyempcde ID \n" +
                                        "From VW_PYEMPMAS a,VW_PYCODMAS b,VW_SYUSRMAS c\n" +
                                        "where a.pycomcde = '200'\n" +
                                        "And a.pycomcde = c.divncode\n" +
                                        "And b.pycomcde = '999'\n" +
                                        "And b.pyhdrcde = 'ST'\n" +
                                        "And nvl(b.pydoppro,'N') = 'Y' \n" +
                                        "And valdflag = 'A'\n" +
                                        "--And pydepcde = (select PYDEPCDE from VW_PYEMPMAS where PYEMPCDE=:APP_USER)\n" +
                                        "And b.pyhdrcde||b.pysofcde = a.pystatus\n" +
                                        "And a.pyempcde Not In ('"+emp_id+"')\n" +
                                        "And a.pyempcde ='"+replacement_id+"'\n" +
                                        "--And PYGRDCDE = (select PYGRDCDE from VW_PYEMPMAS where PYEMPCDE=:APP_USER)\n" +
                                        "Order BY 1";

                                    ps = con.prepareStatement(sql);
                                    rs = ps.executeQuery();

                                    while(rs.next()){

                                        name=rs.getString("NAME");
                                        ID= rs.getString("ID");
                                        row_count++;


                                    }


                                    if(row_count>0){

                                            json.put("Replacement_Name",name);
                                            json.put("Replacement_ID",ID);
                                            json.put("Replacement_Status","Leave");

                                        }
                                    else{

                                            json.put("Replacement_Name",name);
                                            json.put("Replacement_ID",ID);
                                            json.put("Replacement_Status","Inactive");


                                        }

                            }





                        }

                        else if (action.equals("Lv.App.06") && loop_count==1) {

                            loop_count++;
                            leave_type = request.getParameter("leave_type").trim();
                            String start_date = request.getParameter("start_date").trim();
                            String end_date = request.getParameter("end_date").trim();

                            SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");

                            java.util.Date parsed = date_format.parse(start_date);
                            Date sql_start_date = new Date(parsed.getTime());

                            parsed = date_format.parse(end_date);
                            Date sql_end_date = new Date(parsed.getTime());

                            String device_id= request.getParameter("device_id");
                            String session_id= request.getParameter("session_id");
                            String leave_purpose=request.getParameter("leave_purpose");
                            String contact_no= request.getParameter("contact_no");
                            String address= request.getParameter("address");
                            String replacement_id= request.getParameter("replacement_id");
                            //String lfa_type= request.getParameter("lfa_type").trim();
                            String lfa_flag="N";
                            String lfa_lev="N";
                            String foreign_flag= request.getParameter("foreign_flag");
                            String country_name= request.getParameter("country_name");

        //                    if ( lfa_type.equals("LFA01" ) || lfa_type.equals("LFA03" )){
        //                        lfa_flag="Y";
        //                        lfa_lev="N";
        //                    
        //                    }
        //                    else if(lfa_type.equals("LFA02" )){
        //                        
        //                        lfa_flag="Y";
        //                        lfa_lev="Y";
        //                    
        //                    }

                            out.println("Employee-id: " + emp_id);
                            out.println("Leave_type " + leave_type);
                            out.println("Start_date " + sql_start_date);
                            out.println("End_date " + sql_end_date);
                            out.println("Replacement"+replacement_id);
                            //out.println("LFA_TYPE"+lfa_type);

                            if(leave_type.equals("CL"))
                                leave_type="LTCL001";
                            else if(leave_type.equals("EL"))
                                leave_type="LTEL001";
                            else if(leave_type.equals("LFA")){
                                leave_type="LTEL001";
                                lfa_flag="Y";

                            }
                            else if(leave_type.equals("ML"))
                                leave_type="LTMT001";



                            CallableStatement statement = con.prepareCall("{call DPR_MOB_PYLEVDTL_INSERT(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

                            statement.setString(1, "200");
                            statement.setString(2, emp_id);
                            statement.setString(3, session_id);
                            statement.setString(4, device_id);
                            statement.setString(5, leave_type);
                            statement.setString(6, lfa_flag);
                            statement.setString(7, "N");
                            statement.setString(8, foreign_flag);
                            statement.setString(9, country_name);
                            statement.setDate(10, sql_start_date);
                            statement.setDate(11, sql_end_date);
                            statement.setString(12, replacement_id);
                            statement.setString(13, null);
                            statement.setString(14, leave_purpose);
                            statement.setString(15, address);
                            statement.setString(16, contact_no);

                            statement.registerOutParameter(17, Types.VARCHAR);
                            statement.registerOutParameter(18, Types.VARCHAR);


                            statement.execute();

                            String out_messege = statement.getString(17);
                            String out_flag = statement.getString(18);


                            //String leave_doc = statement.getString(19);

                            statement.close();


                            out.println("Leave Flag: " + out_flag);
                            out.println("Leave Message: " + out_messege);

                            json.put("Flag",out_flag);
                            json.put("Message",out_messege);

                        }

                        if (loop_count == 0) {

                        }

                    }



                    rs.close();
                    ps.close();



                    out.println(json);
            
           }

            if (action.equals("Performance.individual")) { // watch closely 

                JSONObject orderedJson = new JSONObject(map_json);
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(Arrays.asList(orderedJson));

                //JSONObject json= new JSONObject();
                json.put("Number of Rows", row_count - 1);
                json.put("Query", jsonArray);

            } else if (action.equals("Leave.03")) {

                OrderedJSONObject json_temp = new OrderedJSONObject();
                json_temp.put("employee_Id", emp_id);
                json_temp.put("Number of Rows", row_count - 1);
                json_temp.put("Query", json);
                json = json_temp;

            } else if (action.equals("Leave.14")) {

                OrderedJSONObject json_temp = new OrderedJSONObject();

                int i = 0;
                for (String temp : approval_person) {

                    if (temp == null) {
                        continue;
                    }

                    i++;
                    json_temp.put(Integer.toString(i), temp);

                }

                json.put("Employee ID", emp_id);
                json.put("Total_Approved_Person", i);
                json.put("Description", json_temp);

            } else if (action.equals("Leave.15")) {

                OrderedJSONObject json_temp = new OrderedJSONObject();
                json_temp.put("employee_Id", emp_id);
                json_temp.put("Number of Leaves", row_count - 1);
                json_temp.put("Query", json);
                json = json_temp;

            } else if (action.equals("Leave.16") || action.equals("Leave.17")) {
              

                OrderedJSONObject json_temp = new OrderedJSONObject();
                json_temp.put("Error_Flag", Error_Flag);
                json_temp.put("Error_Desc", Error_Desc);
                json_temp.put("Number_of_distict_emp", no_of_employees);
                json_temp.put("Number of Records", row_count - 1);
                json_temp.put("Query", json);
                json = json_temp;

            }

            response.addHeader("Access-Control-Allow-Origin", "*");
            response.setCharacterEncoding("UTF-8");

            if ("json".equals(format)) {
                response.setContentType("application/json");
                response.getWriter().print(json.toString());
            } else {
                response.setContentType("text/plain");
                response.getWriter().print(result);
            }
            response.getWriter().flush();

        } catch (SQLException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnectionHandler.releaseConnection(con);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
